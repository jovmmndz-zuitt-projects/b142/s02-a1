package com.mendez.b142.s02.s02app.repositories;

import com.mendez.b142.s02.s02app.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Object> {

}
