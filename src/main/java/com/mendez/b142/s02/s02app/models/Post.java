package com.mendez.b142.s02.s02app.models;

import javax.persistence.*;
/*import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;*/

@Entity
@Table(name="posts") //plural of model name
public class Post {
    // Properties (Columns)
    @Id
    @GeneratedValue
    private Long id; // primary key
    @Column
    private String title;
    @Column
    private String content;

    // Constructors
    // Data: title, content
    // Empty
    public Post(){}

    // Parameterized
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    // Getters & Setters
    public String getTitle() {
        return title;
    }
    public String getContent() {
        return content;
    }

    public void setTitle(String newTitle) {
        this.title = newTitle;
    }
    public void setContent(String newContent) {
        this.content = newContent;
    }

    // Methods

}
